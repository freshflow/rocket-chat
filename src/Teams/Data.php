<?php

namespace ATDev\RocketChat\Teams;

use stdClass;

/**
 * Group data trait
 */
trait Data
{
    /** @var string Team id */
    private $teamId;

    /** @var string Team name */
    private $name;

    /** @var int Privacy of the team (0 - Public, 1 - Private). */
    private $type;

    /** @var array The user ids to add to the team when it is created. */
    private $members;

    /** @var string Room id */
    private $roomId;

    /* Readonly properties returned from api */
    /** @var string Date-time user created at api */
    private $createdAt;

    /**
     * Creates user out of api response
     *
     * @param stdClass $response
     *
     * @return Data
     */
    public static function createOutOfResponse($response)
    {
        $team = new static($response->_id);

        return $team->updateOutOfResponse($response);
    }

    /**
     * Class constructor
     *
     * @param string|null $teamId
     */
    public function __construct($teamId = null)
    {
        if (!empty($teamId)) {
            $this->setTeamId($teamId);
        }
    }

    /**
     * Sets team id
     *
     * @param string $teamId
     *
     * @return Data
     */
    public function setTeamId($teamId)
    {
        if (!(is_null($teamId) || is_string($teamId))) {
            $this->setDataError("Invalid team Id");
        } else {
            $this->teamId = $teamId;
        }

        return $this;
    }

    /**
     * Gets team id
     *
     * @return string
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @param $roomId
     * @return $this
     */
    public function setRoomId($roomId)
    {
        if (!(is_null($roomId) || is_string($roomId))) {
            $this->setDataError("Invalid room Id");
        } else {
            $this->roomId = $roomId;
        }

        return $this;
    }

    /**
     * Gets room id
     *
     * @return string
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Sets team display name
     *
     * @param string $name
     *
     * @return Data
     */
    public function setName($name)
    {
        if (!(is_null($name) || is_string($name))) {
            $this->setDataError("Invalid name");
        } else {
            $this->name = $name;
        }

        return $this;
    }

    /**
     * Gets team display name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets team type
     *
     * @param int $type
     *
     * @return Data
     */
    public function setType($type)
    {
        if (!(is_null($type) || is_numeric($type))) {
            $this->setDataError("Invalid name");
        } else {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * Gets user type at the api
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets team members
     *
     * @param array $members
     *
     * @return Data
     */
    public function setMembers($members)
    {
        if (!(is_null($members) || is_array($members))) {
            $this->setDataError("Invalid members value");
        } else {
            $this->members = $members;
        }

        return $this;
    }

    /**
     * Gets team members
     *
     * @return array
     */
    public function getMembers()
    {
        return $this->members ?: [];
    }

    /**
     * Sets the date-time user created at api
     *
     * @param string $createdAt
     *
     * @return Data
     */
    private function setCreatedAt($createdAt)
    {
        if (is_string($createdAt)) {
            $this->createdAt = $createdAt;
        }

        return $this;
    }

    /**
     * Gets the date-time user created at api
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Updates current user out of api response
     *
     * @param stdClass $response
     *
     * @return Data
     */
    public function updateOutOfResponse($response)
    {
        if (isset($response->_id)) {
            $this->setTeamId($response->_id);
        }

        if (isset($response->name)) {
            $this->setName($response->name);
        }

        if (isset($response->type)) {
            $this->setType($response->type);
        }

        if (isset($response->createdAt)) {
            $this->setCreatedAt($response->createdAt);
        }

        if (isset($response->members)) {
            $this->setMembers($response->members);
        }

        if (isset($response->roomId)) {
            $this->setRoomId($response->roomId);
        }

        return $this;
    }

    /**
     * Sets data error
     *
     * @param string $error
     *
     * @return Data
     */
    private function setDataError($error)
    {
        static::setError($error);

        return $this;
    }
}
