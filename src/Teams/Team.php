<?php

namespace ATDev\RocketChat\Teams;

use ATDev\RocketChat\Common\Request;
use ATDev\RocketChat\Rooms\Room;
use ATDev\RocketChat\Users\User;

class Team extends Request
{
    use \ATDev\RocketChat\Teams\Data;

    const TYPE_PUBLIC = 0;
    const TYPE_PRIVATE = 1;

    /**
     * Gets teams listing
     *
     * @param int $offset
     * @param int $count
     * @return Collection|bool
     */
    public static function listing($offset = null, $count = null)
    {
        $parameters = [];
        if (!is_null($offset)) {
            $parameters['offset'] = $offset;
        }
        if (!is_null($count)) {
            $parameters['count'] = $count;
        }

        static::send("teams.list", "GET", $parameters);

        if (!static::getSuccess()) {
            return false;
        }

        $teams = new Collection();
        $response = static::getResponse();
        foreach ($response->teams as $team) {
            $teams->add(static::createOutOfResponse($team));
        }
        if (isset($response->total)) {
            $teams->setTotal($response->total);
        }
        if (isset($response->count)) {
            $teams->setCount($response->count);
        }
        if (isset($response->offset)) {
            $teams->setOffset($response->offset);
        }

        return $teams;
    }

    /**
     * Lists all of the private teams of any users. The calling user requires to have 'view-room-administration' right
     *
     * @param int $offset
     * @param int $count
     * @return Collection|false
     */
    public static function listAll($offset = 0, $count = 0)
    {
        static::send('teams.listAll', 'GET', ['offset' => $offset, 'count' => $count]);

        if (!static::getSuccess()) {
            return false;
        }

        $teams = new Collection();
        $response = static::getResponse();
        foreach ($response->teams as $team) {
            $teams->add(static::createOutOfResponse($team));
        }
        if (isset($response->total)) {
            $teams->setTotal($response->total);
        }
        if (isset($response->count)) {
            $teams->setCount($response->count);
        }
        if (isset($response->offset)) {
            $teams->setOffset($response->offset);
        }

        return $teams;
    }

    /**
     * Creates group at api instance
     *
     * @return Team|boolean
     */
    public function create()
    {
        $data = [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'members' => $this->getMembers(),
            //room => room configuration
        ];

        //TODO - check automatic room creating when using $this as source for data

        static::send("teams.create", "POST", $data);

        if (!static::getSuccess()) {
            return false;
        }

        return $this->updateOutOfResponse(static::getResponse()->team);
    }

    public function convertToChannel()
    {
        //TODO /api/v1/teams.convertToChannel
    }

    /**
     * Adds rooms to the team. Requires add-team-channel permission.
     *
     * @return \ATDev\RocketChat\Rooms\Collection|boolean
     */
    public function addRooms(array $rooms = [])
    {
        static::send("teams.addRooms", "POST", [
            'teamId' => $this->getTeamId(),
            'rooms' => $rooms,
        ]);

        if (!static::getSuccess()) {
            return false;
        }

        $response = static::getResponse();
        $rooms = new \ATDev\RocketChat\Rooms\Collection();
        if (isset($response->rooms)) {
            foreach ($response->rooms as $room) {
                $rooms->add(Room::createOutOfResponse($room));
            }
        }

        return $rooms;
    }

    /**
     * Removes a room from a team. Requires remove-team-channel permission.
     */
    public function removeRoom($roomId)
    {
        static::send("teams.removeRoom", "POST", [
            'teamId' => $this->getTeamId(),
            'roomId' => $roomId,
        ]);

        if (!static::getSuccess()) {
            return false;
        }

        return $this;
    }

    /**
     * Updates a room from a team. Requires edit-team-channel permission.
     *
     * @param $roomId
     * @param bool $isDefault
     * @return $this|false
     */
    public function updateRoom($roomId, $isDefault = true)
    {
        static::send("teams.updateRoom", "POST", [
            'roomId' => $roomId,
            'isDefault' => $isDefault,
        ]);

        if (!static::getSuccess()) {
            return false;
        }

        return $this;
    }

    /**
     * List all rooms of the team.
     *
     * @param null $filter
     * @param null $type
     * @param null $offset
     * @param null $count
     * @return \ATDev\RocketChat\Rooms\Collection|false
     */
    public function listRooms($filter = null, $type = null, $offset = null, $count = null)
    {
        $parameters = [];
        $parameters['teamId'] = $this->getTeamId();

        if (!is_null($filter)) {
            $parameters['filter'] = $filter;
        }

        if (!is_null($type)) {
            $parameters['type'] = $type;
        }

        if (!is_null($offset)) {
            $parameters['offset'] = $offset;
        }
        if (!is_null($count)) {
            $parameters['count'] = $count;
        }

        static::send("teams.listRooms", "GET", $parameters);

        if (!static::getSuccess()) {
            return false;
        }

        $rooms = new \ATDev\RocketChat\Rooms\Collection();
        $response = static::getResponse();
        foreach ($response->rooms as $room) {
            $rooms->add(Room::createOutOfResponse($room));
        }
        if (isset($response->total)) {
            $rooms->setTotal($response->total);
        }
        if (isset($response->count)) {
            $rooms->setCount($response->count);
        }
        if (isset($response->offset)) {
            $rooms->setOffset($response->offset);
        }

        return $rooms;
    }

    /**
     * @param $userId
     * @param null $offset
     * @param null $count
     * @return \ATDev\RocketChat\Rooms\Collection|false
     */
    public function listRoomsOfUser($userId, $offset = null, $count = null)
    {
        $parameters = [];
        $parameters['teamId'] = $this->getTeamId();
        $parameters['userId'] = $userId;

        if (!is_null($offset)) {
            $parameters['offset'] = $offset;
        }
        if (!is_null($count)) {
            $parameters['count'] = $count;
        }

        static::send("teams.listRoomsOfUser", "GET", $parameters);

        if (!static::getSuccess()) {
            return false;
        }

        $rooms = new \ATDev\RocketChat\Rooms\Collection();
        $response = static::getResponse();
        foreach ($response->rooms as $room) {
            $rooms->add(Room::createOutOfResponse($room));
        }
        if (isset($response->total)) {
            $rooms->setTotal($response->total);
        }
        if (isset($response->count)) {
            $rooms->setCount($response->count);
        }
        if (isset($response->offset)) {
            $rooms->setOffset($response->offset);
        }

        return $rooms;
    }

    /**
     * Lists the users of participants of a private group
     *
     * @param int $offset
     * @param int $count
     * @return \ATDev\RocketChat\Users\Collection|false
     */
    public function members($offset = 0, $count = 0)
    {
        static::send(
            'teams.members',
            'GET',
            array_merge(self::requestParams($this), ['offset' => $offset, 'count' => $count])
        );
        if (!static::getSuccess()) {
            return false;
        }

        $members = new \ATDev\RocketChat\Users\Collection();
        $response = static::getResponse();
        if (isset($response->members)) {
            foreach ($response->members as $user) {
                $members->add(User::createOutOfResponse($user));
            }
        }
        if (isset($response->total)) {
            $members->setTotal($response->total);
        }
        if (isset($response->count)) {
            $members->setCount($response->count);
        }
        if (isset($response->offset)) {
            $members->setOffset($response->offset);
        }

        return $members;
    }

    /**
     * Adds members to the team.
     *
     * Requires add-team-member or edit-team-member permissions.
     *
     * @param array $members
     * @return $this|false
     */
    public function addMembers(array $members = [])
    {
        static::send("teams.addMembers", "POST", [
            'teamId' => $this->getTeamId(),
            'members' => $members,
        ]);

        if (!static::getSuccess()) {
            return false;
        }

        return $this;
    }

    /**
     * Updates a team member's roles.
     *
     * Requires edit-team-member permission.
     *
     * @param $member
     * @return $this|false
     */
    public function updateMember($member)
    {
        static::send("teams.updateMember", "POST", [
            'teamId' => $this->getTeamId(),
            'member' => $member,
        ]);

        if (!static::getSuccess()) {
            return false;
        }

        return $this;
    }

    /**
     * Removes a member from a team.
     *
     * Requires edit-team-member permission.
     *
     * @param $userId
     * @param array $rooms
     * @return $this|false
     */
    public function removeMember($userId, array $rooms = [])
    {
        $params = [
            'teamId' => $this->getTeamId(),
            'userId' => $userId,
        ];

        if (count($rooms)) {
            $params['rooms'] = $rooms;
        }

        static::send("teams.removeMember", "POST", $params);

        if (!static::getSuccess()) {
            return false;
        }

        return $this;
    }

    /**
     * Causes the caller to be removed from the team.
     *
     * @return Team|false
     */
    public function leave()
    {
        static::send('teams.leave', 'POST', ['teamId' => $this->getTeamId()]);
        if (!static::getSuccess()) {
            return false;
        }

        return $this->updateOutOfResponse(static::getResponse()->group);
    }

    /**
     * Gets team info
     *
     * @return Team|boolean
     */
    public function info()
    {
        static::send("teams.info", "GET", ["teamId" => $this->getTeamId()]);

        if (!static::getSuccess()) {
            return false;
        }

        return $this->updateOutOfResponse(static::getResponse()->teamInfo);
    }

    /**
     * Deletes team
     *
     * @return Team|boolean
     */
    public function delete()
    {
        static::send("teams.delete", "POST", ["teamId" => $this->getTeamId()]);

        if (!static::getSuccess()) {
            return false;
        }

        return $this->setTeamId(null);
    }

    /**
     * @param $name
     * @return boolean
     */
    public static function deleteByName($name)
    {
        static::send("teams.delete", "POST", ["teamName" => $name]);

        if (!static::getSuccess()) {
            return false;
        }

        return true;
    }

    /**
     * @param $name
     * @return Collection|false
     */
    public function autocomplete($name)
    {
        static::send("teams.autocomplete", "GET", [
            'name' => $name,
        ]);

        if (!static::getSuccess()) {
            return false;
        }

        $teams = new Collection();
        $response = static::getResponse();
        foreach ($response->teams as $team) {
            $teams->add(static::createOutOfResponse($team));
        }

        return $teams;
    }

    /**
     * @param $name
     * @param $type
     * @return Team|false
     */
    public function update($name, $type)
    {
        static::send(
            'teams.update', 'POST', [
                'teamId' => $this->getTeamId(),
                'data' => [
                    'name' => $name,
                    'type' => $type,
                ]
            ]
        );

        if (!static::getSuccess()) {
            return false;
        }

        return $this;
    }

    /**
     * Prepares request params to have `roomId` or `roomName`
     *
     * @param Team|null $group
     * @return array
     */
    private static function requestParams(Team $team = null)
    {
        $params = [];

        if (isset($team) && !empty($team->getRoomId())) {
            $params = ['roomId' => $team->getRoomId()];
        }

        if (isset($team) && !empty($team->getTeamId())) {
            $params = ['teamId' => $team->getTeamId()];
        }

        return $params;
    }
}