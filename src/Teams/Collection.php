<?php

namespace ATDev\RocketChat\Teams;

/**
 * Group collection class
 */
class Collection extends \ATDev\RocketChat\Common\Collection
{

    /**
     * @param Team $element
     * @return bool|true
     */
    public function add($element)
    {
        if (!($element instanceof Team)) {
            return false;
        }

        return parent::add($element);
    }
}
