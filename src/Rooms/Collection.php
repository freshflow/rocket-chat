<?php

namespace ATDev\RocketChat\Rooms;

/**
 * Group collection class
 */
class Collection extends \ATDev\RocketChat\Common\Collection
{

    /**
     * @param Room $element
     * @return bool|true
     */
    public function add($element)
    {
        if (!($element instanceof Room)) {
            return false;
        }

        return parent::add($element);
    }
}
